﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Add-ExcelShapePicture" {
    Context "Unit Tests" {
        It "Adds a picture to excel worksheet." {
            $ImageFilePath = "$TestDrive\temp.png"

            Add-Type -AssemblyName System.Drawing
            $image = [System.Drawing.Bitmap]::new(250, 150)
            $graphics = [System.Drawing.Graphics]::FromImage($image)

            # 背景をグレーで塗りつぶす
            $graphics.FillRectangle([System.Drawing.Brushes]::LightGray, $graphics.VisibleClipBounds)

            # 黒色で四角形を描画
            $graphics.DrawRectangle([System.Drawing.Pens]::Black, 50, 50, 50, 50)

            $graphics.Dispose()
            $image.Save($ImageFilePath)
            $image.Dispose()


            $ShapeType = $Null
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = $Excel.Workbooks.Add()
                $Sheet = $Book.Worksheets(1)
                $Shape = Add-ExcelShapePicture -Worksheet $Sheet -FileName $ImageFilePath
                $ShapeType = $Shape.Type
            } finally {
                If ($Book) {
                    $Book.Close($False)
                    $Book = $Null
                }
                If ($Excel) {
                    $Excel.Quit()
                    $Excel = $Null
                    [GC]::Collect()
                }
            }
            $ShapeType | Should Be 13 # 13: msoPicture
        }
    }
}
