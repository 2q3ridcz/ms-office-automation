﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Save-PowerPointAs" {
    Context "Simple usecase" {
        It "Saves a newly created presentation" {
            $PresentationFilePath = "$TestDrive\" + "Saves a newly created presentation".Replace(" ","-") + ".pptx"

            $Result = "Failed"
            try {
                $PowerPoint = New-PowerPointApplication -Visible $False -Verbose
                $Presentation = $PowerPoint.Presentations.Add()
                $Presentation.Slides.Add(1, 1) # スライドのインデックスは1から始まります。1は通常のスライドです。

                $PresentationFilePath | Should Not Exist
                $Presentation | Save-PowerPointAs -FileName $PresentationFilePath
                $PresentationFilePath | Should Exist

                $Result = "Success"
            } finally {
                If ($Presentation) {$Presentation.Close(); $Presentation = $Null}
                If ($PowerPoint) {$PowerPoint.Quit(); $PowerPoint = $Null}
                [GC]::Collect()
            }
            $Result | Should Be "Success"
        }
    }
}
