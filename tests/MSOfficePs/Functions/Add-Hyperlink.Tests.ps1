﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Add-Hyperlink" {
    Context "Unit Test" {
        It "Adds a hyperlink to excel worksheet" {
            $Path = "$TestDrive\" + "Adds a hyperlink to excel worksheet".Replace(" ","-") + ".xlsx"

            $Checkpoints = @()
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = $Excel.Workbooks.Add()
                $LinkSheetName = $Book.Worksheets.Item(1).Name
                $Sheet = $Book.Worksheets.Add()
                $Sheet.Hyperlinks.Count | Should Be 0

                $FuncParam = @{
                    "Worksheet" = $Sheet
                    "Anchor" = $Sheet.Cells.Item(1,1)
                    "Address" = ""
                    "SubAddress" = ("'" + $LinkSheetName + "'!A1")
                    "ScreenTip" = $LinkSheetName
                    "TextToDisplay" = $LinkSheetName
                }
                Add-Hyperlink @FuncParam | Out-Null
                $Book | Save-ExcelAs -FileName $Path

                $Sheet.Hyperlinks.Count | Should Be 1
                $Sheet.Hyperlinks.Item(1).Name | Should Be $FuncParam.TextToDisplay
                $Sheet.Hyperlinks.Item(1).SubAddress | Should Be $FuncParam.SubAddress
                $Sheet.Hyperlinks.Item(1).ScreenTip | Should Be $FuncParam.ScreenTip
                $Sheet.Hyperlinks.Item(1).TextToDisplay | Should Be $FuncParam.TextToDisplay

                $Checkpoints += "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
                $Checkpoints += "Success"
            }
            $Checkpoints.Length | Should Be 2
            $Checkpoints | Should Be "Success"
        }
    }
}
