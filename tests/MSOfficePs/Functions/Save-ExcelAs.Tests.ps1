﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Save-ExcelAs" {
    Context "FileFormat" {
        $TestCases = @(
            @{"TestCase" = "CSV"; "FileFormat" = "6"}
            ### Comment out testcases because they don't work.
            # @{"TestCase" = "UTF8 CSV"; "FileFormat" = "62"}
            # @{"TestCase" = "Windows CSV"; "FileFormat" = "23"}
        )
        It "Saves as <TestCase> (FileFormat=<FileFormat>)" -TestCases $TestCases {
            param ($TestCase, $FileFormat)
            $BookFilePath = "$TestDrive\" + "Saves as $TestCase (FileFormat=$FileFormat)".Replace(" ","-") + ".xlsx"

            $Result = $Null
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = $Excel.Workbooks.Add()
                $Sheet = $Book.Worksheets(1)
                $Sheet.Cells.Item(1,1).Value2 = "Col-1"
                $Sheet.Cells.Item(1,2).Value2 = "Col-2"
                $Sheet.Cells.Item(1,3).Value2 = "Col-3"
                $Sheet.Cells.Item(2,1).Value2 = "値1"
                $Sheet.Cells.Item(2,2).Value2 = "値2"
                $Sheet.Cells.Item(2,3).Value2 = "値3"

                $BookFilePath | Should Not Exist
                $Book | Save-ExcelAs -FileName $BookFilePath -FileFormat $FileFormat
                $BookFilePath | Should Exist

                $Obj = @(Import-Csv -Path $BookFilePath -Encoding Default)
                $Obj[0]."Col-1" | Should Be "値1"
                $Obj[0]."Col-2" | Should Be "値2"
                $Obj[0]."Col-3" | Should Be "値3"

                $Result = "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
            }
            $Result | Should Be "Success"
        }
    }

    Context "ReadOnlyRecommended" {
        $TestCases = @(
            @{"TestCase" = "null"; "ReadOnlyRecommended" = $Null}
        )
        It "Throws when ReadOnlyRecommended is <TestCase>" -TestCases $TestCases {
            param ($ReadOnlyRecommended)
            $BookFilePath = "$TestDrive\" + "Throws when ReadOnlyRecommended is $TestCase".Replace(" ","-") + ".xlsx"

            $Result = $Null
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = $Excel.Workbooks.Add()
                { Save-ExcelAs -FileName $BookFilePath -ReadOnlyRecommended $ReadOnlyRecommended } | Should Throw
                $Result = "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
            }
            $Result | Should Be "Success"
        }

        $TestCases = @(
            @{"TestCase" = "true"; "ReadOnlyRecommended" = $True}
            @{"TestCase" = "false"; "ReadOnlyRecommended" = $False}
        )
        It "Sets Book.ReadOnlyRecommended to <ReadOnlyRecommended> when ReadOnlyRecommended is <TestCase>" -TestCases $TestCases {
            param ($ReadOnlyRecommended)
            $BookFilePath = "$TestDrive\" + "Sets Book.ReadOnlyRecommended to $ReadOnlyRecommended when ReadOnlyRecommended is $TestCase".Replace(" ","-") + ".xlsx"

            $Result = $Null
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = $Excel.Workbooks.Add()

                $BookFilePath | Should Not Exist
                $Book.ReadOnlyRecommended | Should Be $False

                $Book | Save-ExcelAs -FileName $BookFilePath -ReadOnlyRecommended $ReadOnlyRecommended

                $Book.ReadOnlyRecommended | Should Be $ReadOnlyRecommended
                $BookFilePath | Should Exist
                $Result = "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
            }
            $Result | Should Be "Success"
        }
    }
}
