﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Merge-ExcelFile" {
    Context "MergeFilePathList" {
        BeforeAll {
            function New-ExcelBookWithSheets ($Excel, $Path, $SheetNameList) {
                $Book = $Excel.Workbooks.Add()
                While ($SheetNameList.Length -Lt $Book.Worksheets.Count) {
                    $Book.Worksheets.Item($Book.Worksheets.Count).Delete()
                }
                While ($Book.Worksheets.Count -Lt $SheetNameList.Length) {
                    $Book.Worksheets.Add() | Out-Null
                }
                foreach ($i in @(0..($SheetNameList.Length - 1))) {
                    $Book.Worksheets.Item($i + 1).Name = $SheetNameList[$i]
                }
                $Book | Save-ExcelAs -FileName $Path
                If ($Book) {$Book.Close($False); $Book = $Null}
            }

            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose

                $BaseBookFilePath = "$TestDrive\" + "base-book.xlsx"
                $BaseBookSheetNameList = @("one-of-base","two-of-base")
                New-ExcelBookWithSheets -Excel $Excel -Path $BaseBookFilePath -SheetNameList $BaseBookSheetNameList

                $OneSheetBookFilePath = "$TestDrive\" + "one-sheet-book.xlsx"
                $OneSheetBookSheetNameList = @("one-of-one")
                New-ExcelBookWithSheets -Excel $Excel -Path $OneSheetBookFilePath -SheetNameList $OneSheetBookSheetNameList

                $TwoSheetBookFilePath = "$TestDrive\" + "two-sheets-book.xlsx"
                $TwoSheetBookSheetNameList = @("one-of-two","two-of-two")
                New-ExcelBookWithSheets -Excel $Excel -Path $TwoSheetBookFilePath -SheetNameList $TwoSheetBookSheetNameList

            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
            }
        }

        $TestCases = @(
            @{
                "TestCase" = "one book with one sheet";
                "MergeFilePathList" = @($BaseBookFilePath,$OneSheetBookFilePath)
                "Expect" = @() + $BaseBookSheetNameList + $OneSheetBookSheetNameList;
            }
            @{
                "TestCase" = "one book with two sheets";
                "MergeFilePathList" = @($BaseBookFilePath,$TwoSheetBookFilePath);
                "Expect" = @() + $BaseBookSheetNameList + $TwoSheetBookSheetNameList;
            }
            @{
                "TestCase" = "two books with one sheet each";
                "MergeFilePathList" = @($BaseBookFilePath,$OneSheetBookFilePath,$OneSheetBookFilePath);
                "Expect" = @() + $BaseBookSheetNameList + $OneSheetBookSheetNameList + @($OneSheetBookSheetNameList | %{ $_ + " (2)" });
            }
            @{
                "TestCase" = "two books with two sheets each";
                "MergeFilePathList" = @($BaseBookFilePath,$TwoSheetBookFilePath,$TwoSheetBookFilePath);
                "Expect" = @() + $BaseBookSheetNameList + $TwoSheetBookSheetNameList + @($TwoSheetBookSheetNameList | %{ $_ + " (2)" });
            }
        )
        It "Merges <TestCase> to the base book" -TestCases $TestCases {
            param ($TestCase,$MergeFilePathList,$Expect)

            $MergeFilePathList | Should Exist

            $Path = "$TestDrive\" + "Merges $TestCase to the base book".Replace(" ","-") + ".xlsx"
            $Path | Should Not Exist
            Merge-ExcelFile -Path $Path -MergeFilePathList $MergeFilePathList
            $Path | Should Exist

            $Checkpoints = @()
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = Open-ExcelBook -Excel $Excel -FileName $Path -Verbose
                Assert-List -Result @($Book.Worksheets | %{$_.Name}) -Expect $Expect
                $Checkpoints += "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
                $Checkpoints += "Success"
            }
            $Checkpoints.Length | Should Be 2
            $Checkpoints | Should Be "Success"
        }
    }
}
