﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Open-ExcelBook" {
    Context "FileName" {
        $TestCases = @(
            ### Comment out two testcases because they cause memory leak (unfinished "et" and "wps" processes)
            # @{"TestCase" = "null"; "FileName" = $Null}
            # @{"TestCase" = "empty string"; "FileName" = ""}
            @{"TestCase" = "invalid"; "FileName" = "abcdefg"}
        )
        It "Throws when FileName is <TestCase>" -TestCases $TestCases {
            param ($FileName)

            $Checkpoints = @()
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                { $Book = Open-ExcelBook -Excel $Excel -FileName $FileName -Verbose } | Should Throw
                $Checkpoints += "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
                $Checkpoints += "Success"
            }
            $Checkpoints.Length | Should Be 2
            $Checkpoints | Should Be "Success"
        }
    }

    Context "ReadOnly" {
        $TestCases = @(
            @{"TestCase" = "true"; "ReadOnlyRecommended" = $True; "ReadOnly" = $True; "Expect" = $True}
            @{"TestCase" = "false"; "ReadOnlyRecommended" = $True; "ReadOnly" = $False; "Expect" = $True}
            @{"TestCase" = "true"; "ReadOnlyRecommended" = $False; "ReadOnly" = $True; "Expect" = $True}
            @{"TestCase" = "false"; "ReadOnlyRecommended" = $False; "ReadOnly" = $False; "Expect" = $False}
        )
        It "Returns <Expect> when Book.ReadOnlyRecommended is <ReadOnlyRecommended> and ReadOnly is <ReadOnly>" -TestCases $TestCases {
            param ($TestCase, $ReadOnlyRecommended, $ReadOnly, $Expect)

            $BookFilePath = "$TestDrive\" + "Throws when ReadOnly is $TestCase".Replace(" ","-") + ".xlsx"

            $Checkpoints = @()
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = $Excel.Workbooks.Add()
                $Sheet = $Book.Worksheets(1)
                $Sheet.Cells.Item(1,1).Value2 = "Hello world"
                $Book | Save-ExcelAs -FileName $BookFilePath -ReadOnlyRecommended $ReadOnlyRecommended
                $Book.Close($False)
                $Book = $Null

                $Book = Open-ExcelBook -Excel $Excel -FileName $BookFilePath -ReadOnly $ReadOnly -Verbose
                $Book.ReadOnly | Should Be $Expect
                $Sheet = $Book.Worksheets(1)
                $Sheet.Cells.Item(1,1).Value2 | Should Be "Hello world"
                $Checkpoints += "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
                $Checkpoints += "Success"
            }
            $Checkpoints.Length | Should Be 2
            $Checkpoints | Should Be "Success"
        }
    }
}
