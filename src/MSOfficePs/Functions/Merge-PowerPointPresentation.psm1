﻿Function Merge-PowerPointPresentation {
    <#
    .SYNOPSIS
    Merges multiple PowerPoint presentations into a single presentation.

    .DESCRIPTION
    This function combines multiple PowerPoint presentations into one by copying all slides
    from the source presentations into a new presentation file. It handles opening and closing
    PowerPoint applications properly and ensures resources are cleaned up.

    .PARAMETER Path
    The output file path where the merged presentation will be saved.

    .PARAMETER MergeFilePathList
    An array of file paths to the PowerPoint presentations that should be merged.

    .EXAMPLE
    Merge-PowerPointPresentation -Path "C:\Output\Combined.pptx" -MergeFilePathList @("C:\Input\Presentation1.pptx", "C:\Input\Presentation2.pptx")

    .NOTES
    The function will create a visible PowerPoint application instance during the merge process.
    All source presentations are opened in read-only mode to prevent accidental modifications.
    The function ensures proper cleanup of PowerPoint resources after completion.
    #>
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$true)]
        [string]
        $Path
        ,
        [Parameter(Mandatory=$true)]
        [string[]]
        $MergeFilePathList
    )
    begin {}
    process {
        try {
            $PowerPoint = New-PowerPointApplication -Visible $True
            $Presentation = $Null

            $MergeFilePathList | ForEach-Object {
                $MergeFilePath = $_
                if ($Null -eq $Presentation) {
                    $Presentation = Open-PowerPointPresentation -PowerPoint $PowerPoint -FileName $MergeFilePath -ReadOnly $True
                } else {
                    $TempPresentation = Open-PowerPointPresentation -PowerPoint $PowerPoint -FileName $MergeFilePath -ReadOnly $True
                    foreach ($Slide in $TempPresentation.Slides) {
                        $Slide.Copy()
                        $Presentation.Slides.Paste()
                    }
                    $TempPresentation.Close()
                    $TempPresentation = $Null
                }
            }
            Save-PowerPointAs -Presentation $Presentation -FileName $Path
        } finally {
            If ($TempPresentation) {$TempPresentation.Close(); $TempPresentation = $Null}
            If ($Presentation) {$Presentation.Close(); $Presentation = $Null}
            If ($PowerPoint) {$PowerPoint.Quit(); $PowerPoint = $Null}
            [GC]::Collect()
        }
    }
    end {}
}