﻿<#
.SYNOPSIS
    Opens an excel workbook.
.DESCRIPTION
    Opens an excel workbook.

    https://docs.microsoft.com/en-us/office/vba/api/excel.workbooks.open
.EXAMPLE
    Open-ExcelBook -Excel $Excel -FileName "C:\test.xlsx"
#>
Function Open-ExcelBook {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidUsingPlainTextForPassword", "")]
    [Alias()]
    [OutputType([Object])]
    param (
        [Parameter(Mandatory=$true)]
        [object]
        $Excel
        ,
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [string]
        $FileName
        ,
        [Parameter(Mandatory=$false)]
        [ValidateSet(0,3)]
        [object]
        $UpdateLinks = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [ValidateSet($True,$False)]
        [object]
        $ReadOnly = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [ValidateSet(1,2,3,4,5,6)]
        [object]
        $Format = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [object]
        $Password = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [object]
        $WriteResPassword = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [ValidateSet($True,$False)]
        [object]
        $IgnoreReadOnlyRecommended = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [object]
        $Origin = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [object]
        $Delimiter = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [object]
        $Editable = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [object]
        $Notify = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [object]
        $Converter = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [object]
        $AddToMru = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [object]
        $Local = [Type]::Missing
        # ,
        # [Parameter(Mandatory=$false)]
        # [object]
        # $CorruptLoad = [Type]::Missing
    )
    begin {}
    process {
        $BeforeProcess = Get-Process

        ### Comment out Corrupt Load. It works fine with WPS Office 11.
        ### But it causes COMExecption when using Excel 2016.
        $Excel.Workbooks.Open(
            $FileName,
            $UpdateLinks,
            $ReadOnly,
            $Format,
            $Password,
            $WriteResPassword,
            $IgnoreReadOnlyRecommended,
            $Origin,
            $Delimiter,
            $Editable,
            $Notify,
            $Converter,
            $AddToMru,
            $Local # ,
            # $CorruptLoad
        )

        $AfterProcess = Get-Process
        $AfterProcess |
        ?{ $BeforeProcess.Id -NotContains $_.Id } |
        %{ "" + $_.Id + " " + $_.ProcessName + " (Open-ExcelBook)" } |
        Write-Verbose
    }
    end {}
}
