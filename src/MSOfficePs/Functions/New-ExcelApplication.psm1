﻿<#
.SYNOPSIS
    Starts an excel com object.

.DESCRIPTION
    Creates an excel com object.
    If Excel is not installed, it will try to return WPS Office object.
    If both is not installed, it will throw an error.
.EXAMPLE
    $Excel = New-ExcelApplication
#>
Function New-ExcelApplication {
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$false)]
        [bool]
        $Visible = $True
        ,
        [Parameter(Mandatory=$false)]
        [bool]
        $ScreenUpdating = $True
        ,
        [Parameter(Mandatory=$false)]
        [bool]
        $DisplayAlerts = $True
    )
    begin {}
    process {
        $BeforeProcess = Get-Process

        $Excel = $Null
        Try {
            $Excel = New-Object -ComObject Excel.Application
        } Catch [System.Runtime.InteropServices.COMException] {
            Try {
                $Excel = New-Object -ComObject KET.Application
            } Catch [System.Runtime.InteropServices.COMException] {
                Throw "ExcelApplicationNotFoundException"
            }
        }

        $AfterProcess = Get-Process
        $AfterProcess |
        ?{ $BeforeProcess.Id -NotContains $_.Id } |
        %{ "" + $_.Id + " " + $_.ProcessName + " (New-ExcelApplication)" } |
        Write-Verbose

        If ($Excel) {
            $Excel.ScreenUpdating = $ScreenUpdating
            $Excel.DisplayAlerts = $DisplayAlerts
            $Excel.Visible = $Visible
            $Excel | Write-Output
        }
    }
    end {}
}
