﻿Function ConvertFrom-ExcelDateInteger {
    param (
        [Parameter(ValueFromPipeline=$True, Mandatory=$True)]
        [Int]
        $DateInteger
    )

    begin {
        $BaseDate = Get-Date -Date "1899/12/31"
    }

    process {
        If ( $DateInteger -Lt 0 ) {
            Write-Error ("Excel cannot parse negative number to a date.")
        } ElseIf ( $DateInteger -Eq 0 ) {
            Write-Error ("Excel parses '0' to an invalid date(1900-01-01).")
        } ElseIf ( $DateInteger -Lt 60 ) {
            $BaseDate.AddDays($DateInteger)
        } ElseIf ( $DateInteger -Eq 60 ) {
            Write-Error ("Excel parses '60' to an invalid date(1900-02-29).")
        } ElseIf ( 60 -Lt $DateInteger ) {
            $BaseDate.AddDays($DateInteger - 1)
        } Else {
            Throw ("Unhandled exception. DateInteger: " + $DateInteger)
        }
    }
    end {}
}
