﻿Function Merge-ExcelFile {
    param (
        [Parameter(Mandatory=$True)]
        [string]
        $Path
        ,
        [Parameter(Mandatory=$True)]
        [string[]]
        $MergeFilePathList
    )
    begin {}
    process {
        ### get the first book in the list and move all sheets in other books to it.
        $Checkpoint = $Null
        try {
            $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False
            $BaseBook = $Null
            foreach ($BookFilePath in $MergeFilePathList) {
                If ($Null -Eq $BaseBook) {
                    $BaseBook = $Excel.Workbooks.Add($BookFilePath)
                    Continue
                }

                $Book = Open-ExcelBook -Excel $Excel -FileName $BookFilePath -ReadOnly $True
                foreach ($Sheet in $Book.Worksheets) {
                    $Sheet.Move([Type]::Missing, $BaseBook.Worksheets.Item($BaseBook.Worksheets.Count))
                }
                If ($Book.Worksheets.Count -Ne 0) {$Book.Close($False); $Book = $Null}
            }

            $BaseBook | Save-ExcelAs -FileName $Path
            $Checkpoint = "Success"
        } catch {
            Write-Error $_
        } finally {
            If ($Book.Worksheets.Count -Ne 0) {$Book.Close($False); $Book = $Null}
            If ($BaseBook) {$BaseBook.Close($False); $BaseBook = $Null}
            If ($Excel) {$Excel.Quit(); $Excel = $Null}
            [GC]::Collect()
        }

        ### output book path
        If ($Checkpoint -Eq "Success") {
            $Path | Write-Output
        }
    }
    end {}
}
