﻿<#
.SYNOPSIS
    Creates a new PowerPoint application instance.
.DESCRIPTION
    Creates a new PowerPoint application instance using COM objects, supporting both standard PowerPoint and Korean PowerPoint (KWPP).
.PARAMETER Visible
    Boolean value determining if the PowerPoint application window should be visible. Defaults to $True.
.OUTPUTS
    PowerPoint.Application object
.EXAMPLE
    $PowerPoint = New-PowerPointApplication -Visible $false
#>
Function New-PowerPointApplication {
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseShouldProcessForStateChangingFunctions", "")]
    param (
        [Parameter(Mandatory=$false)]
        [ValidateNotNull()]
        [bool]
        $Visible = $True
    )
    begin {}
    process {
        Write-Verbose "Starting PowerPoint application creation"
        $BeforeProcess = Get-Process

        $PowerPoint = $Null
        Try {
            Write-Verbose "Attempting to create standard PowerPoint application"
            $PowerPoint = New-Object -ComObject PowerPoint.Application
        } Catch [System.Runtime.InteropServices.COMException] {
            Write-Verbose "Standard PowerPoint creation failed, attempting Korean PowerPoint"
            Try {
                $PowerPoint = New-Object -ComObject "KWPP.Application"
            } Catch [System.Runtime.InteropServices.COMException] {
                Throw "Unable to create PowerPoint application. Ensure PowerPoint is properly installed."
            }
        }

        $AfterProcess = Get-Process
        $NewProcesses = $AfterProcess | Where-Object { $BeforeProcess.Id -NotContains $_.Id }

        $NewProcesses | ForEach-Object {
            Write-Verbose "New process created: $($_.Id) $($_.ProcessName) (New-PowerPointApplication)"
        }

        If ($PowerPoint) {
            # `$PowerPoint.Visible = $False` causes an error for some reason.
            # Default is $False, so only change when $True is requested.
            If ($Visible) {
                Write-Verbose "Setting PowerPoint visibility to $Visible"
                $PowerPoint.Visible = $Visible
            }
            Write-Verbose "Successfully created PowerPoint application"
            $PowerPoint | Write-Output
        }
    }
    end {}
}
