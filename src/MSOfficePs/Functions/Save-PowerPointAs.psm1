﻿<#
.SYNOPSIS
    Saves a PowerPoint presentation to a specified file.
.DESCRIPTION
    Saves a PowerPoint presentation to a specified location with the given filename.
    Supports saving through COM object interface.
.PARAMETER Presentation
    PowerPoint COM presentation object to be saved.
.PARAMETER FileName
    The full path and filename where the presentation should be saved.
    If path is not specified, saves in the current directory.
.EXAMPLE
    $presentation | Save-PowerPointAs -FileName "C:\Presentations\MyPresentation.pptx"
    Saves the presentation to the specified path.
.EXAMPLE
    Save-PowerPointAs -Presentation $presentation -FileName "MyPresentation.pptx"
    Saves the presentation in the current directory.
.NOTES
    Ensure you have write permissions to the target location.
#>
Function Save-PowerPointAs {
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseSingularNouns", "")]
    [CmdletBinding()]
    [Alias()]
    [OutputType([void])]
    param (
        # PowerPoint COM presentation object.
        [Parameter(Mandatory=$true,
                   ValueFromPipeline=$true,
                   Position=0)]
        [ValidateNotNull()]
        [object]
        $Presentation
        ,
        # Specifies the name to save the file under. If you don't include a full path, PowerPoint saves the file in the current folder.
        [Parameter(Mandatory=$true,
                   Position=1)]
        [string]
        $FileName
        # ,
        # # Specifies the saved file format. If this argument is omitted, the file is saved in the default file format (ppSaveAsDefault).
        # [Parameter(Mandatory=$false,
        #            Position=2)]
        # [object]
        # $FileFormat = [Type]::Missing
        # ,
        # # Specifies whether PowerPoint embeds TrueType fonts in the saved presentation.
        # [Parameter(Mandatory=$false,
        #            Position=3)]
        # [object]
        # $EmbedFonts = [Type]::Missing
    )
    begin {}
    process {
        # If ($PSBoundParameters.ContainsKey("FileFormat")) {
        #     $FileFormat = [Int]$FileFormat
        # }

        $Presentation.SaveAs(
            $FileName
            # , $FileFormat
        )
    }
    end {}
}
