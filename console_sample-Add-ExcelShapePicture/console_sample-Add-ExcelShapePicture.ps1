$here = Split-Path -Parent $MyInvocation.MyCommand.Path
Push-Location -Path $here

$ProjectFolderPath = "$here\..\" | Resolve-Path

$PackageName = "MSOfficePs"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force


$ImageFolderPath = "$here\Input"
$ExcelFilePath = "$here\Output\temp.xlsx"

$ImageFiles = Get-ChildItem -Path $ImageFolderPath | ?{ $_.Extension -Eq ".jpg" }


try {
    $Excel = New-ExcelApplication -Visible $True -DisplayAlerts $False
    $Book = $Excel.Workbooks.Add()
    $Sheet = $Book.Worksheets(1)

    $Left = $Sheet.Cells(1, 1).Left
    $Top = $Sheet.Cells(1, 1).Top
    foreach ($ImageFile in $ImageFiles) {
        $Shape = Add-ExcelShapePicture -Worksheet $Sheet -FileName $ImageFile.FullName -Left $Left -Top $Top
        $NextRow = $Shape.BottomRightCell.Row + 2
        $Top = $Sheet.Cells($NextRow, 1).Top
    }

    Save-ExcelAs -Workbook $Book -FileName $ExcelFilePath
} finally {
    If ($Book) {
        $Book.Close($False)
        $Book = $Null
    }
    If ($Excel) {
        $Excel.Quit()
        $Excel = $Null
    }
    [GC]::Collect()
}

Read-Host -Prompt ("Fin! Press enter to exit...")
